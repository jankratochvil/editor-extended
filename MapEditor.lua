--------------------------------------------------------------------------------
-- This is an official Reflex script. Do not modify.
--------------------------------------------------------------------------------

require "base/internal/ui/reflexcore"

require "socket"

local thumbSize = 70;
local thumbCornerRadius = 6;

MapEditor =
{
	canPosition = false,
	canHide = false,
	--isMenu = false,

	largeIcons = false,
	entityTypeComboBoxData = {},
	pickupComboBoxData = {},
	spotlightTeamIndexComboBoxData = {},
	pointlightTeamIndexComboBoxData = {}
};
-- registerWidget("MapEditor");

function MapEditor:initialize()
    self.userData = loadUserData();

    CheckSetDefaultValue(self, "userData", "table", {});
    CheckSetDefaultValue(self.userData, "bSnapAngle", "boolean", true);
    CheckSetDefaultValue(self.userData, "bSnapAngleControl", "boolean", true);
end
--------------------------------------------------------------------------------
-- combo box pickup with pickup names rather than number
--------------------------------------------------------------------------------
local function uiComboBoxPickup(pickupNumber, x, y, w, comboBoxData, optionalId)
	local selections = {};
	selections[PICKUP_TYPE_WEAPONBURSTGUN] = "Burstgun";
	selections[PICKUP_TYPE_WEAPONSHOTGUN] = "Shotgun";
	selections[PICKUP_TYPE_WEAPONGRENADELAUNCHER] = "Grenade Launcher";
	selections[PICKUP_TYPE_WEAPONPLASMARIFLE] = "Plasma Rifle";
	selections[PICKUP_TYPE_WEAPONROCKETLAUNCHER] = "Rocket Launcher";
	selections[PICKUP_TYPE_WEAPONIONCANNON] = "Ion Cannon";
	selections[PICKUP_TYPE_WEAPONBOLTRIFLE] = "Bolt Rifle";
	-- selections[PICKUP_TYPE_WEAPONSTAKELAUNCHER] = "Stake Launcher"; -- temp: remove stake launcher
	selections[PICKUP_TYPE_HEALTH5] = "Small Health";
	selections[PICKUP_TYPE_HEALTH25] = "Medium Health";
	selections[PICKUP_TYPE_HEALTH50] = "Large Health";
	selections[PICKUP_TYPE_HEALTH100] = "Mega Health";
	selections[PICKUP_TYPE_ARMOR5] = "Armor Shard";
	selections[PICKUP_TYPE_ARMOR50] = "Green Armor";
	selections[PICKUP_TYPE_ARMOR100] = "Yellow Armor";
	selections[PICKUP_TYPE_ARMOR150] = "Red Armor";
	selections[PICKUP_TYPE_POWERUPCARNAGE] = "Carnage";
	selections[PICKUP_TYPE_POWERUPRESIST] = "Resist";
	selections[PICKUP_TYPE_FLAGTEAMA] = "Team A Flag";	-- todo: fix with proper team names
	selections[PICKUP_TYPE_FLAGTEAMB] = "Team B Flag";	-- todo: fix with proper team names

	local selection = selections[pickupNumber];

	local newSelection = uiComboBox(selections, selection, x, y, w, comboBoxData, optionalId);

	for k, v in pairs(selections) do
		if v == newSelection then
			return k;
		end
	end

	return 0;
end


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

local function twoPointLen(a, b, x, y)
	return math.sqrt((a-x)*(a-x)+(b-y)*(b-y))
end

local function uiTeamIndexCombo(teamIndex, x, y, w, comboBoxData, uniqueId, enabled)
	local teamIndexOptions = { "-", "A", "B" };
	
	local currentOption = "-";
	if teamIndex==0 then currentOption = "A" end;
	if teamIndex==1 then currentOption = "B" end;
	
	local newOption = uiComboBox(teamIndexOptions, currentOption, x, y, w, comboBoxData, uniqueId, enabled);
	
	if newOption == "A" then return 0 end;
	if newOption == "B" then return 1 end;
	return -1;
end


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

local function wrap(x, min, max)
	if x < min then
		return max + x;
	elseif x > max then
		return x - max;
	else 
		return x;
	end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

local function snap(x, snapval, min, max)
	if x % snapval > snapval/2 then
		x = math.floor(x / snapval + 1) * snapval;
	else
		x = math.floor(x / snapval) * snapval;
	end
	x = wrap(x, min, max);
	return x;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

local function uiRadialControl(value, x, y, w, bSnapAngle, snapAngle, min, max, optionalId, enabled)
	local h = w;
	local padx = h*0.3;

	local c = 255;
	local t = nil;
	if enabled == false then
		c = UI_DISABLED_TEXT;
		t = {};
		-- t.value = value;
		t.focus = false;
		t.apply = false;
		t.hoverAmount = 0;
	else
		t = mouseRegion(x, y, h, h, optionalId);
		-- t = textRegion(x, y, w, h, value, optionalId);
	end

	nvgSave();
	local cx = x+h/2;
	local cy = y+h/2;
	local r = h/2;
	
	local v = tonumber(value);
	if v ~= nil then
		v = v % max;
	else
		return min;
	end
	-- if v < 0 then
	-- 	v = max - v;
	-- end
	v = wrap(v, min, max);

	local valuerad = v / 180 * math.pi;

	nvgTranslate(cx,cy);

	-- Edit
	nvgBeginPath();
	nvgCircle(0,0, r);
	nvgFillRadialGradient(0, 0+1.5, 0, 6*math.pi, Color(c,c,c,32), Color(32,32,32,32))
	nvgFill();

	local px = r*math.cos(valuerad-math.pi/2)
	local py = r*math.sin(valuerad-math.pi/2);

	nvgBeginPath();
	nvgMoveTo(0,0);
	nvgCircle(0,0,r);
	nvgPathWinding(NVG_HOLE);
	nvgMoveTo(0,-r);
	nvgLineTo(0,0);
	nvgLineTo(px, py);
	if(px<=0 and py <= 0) then
		nvgLineTo(-r,-r);
		nvgClosePath();
	elseif(px<=0 and py >= 0) then
		nvgLineTo(-r,r);
		nvgLineTo(-r,-r);
		nvgClosePath();
	elseif(px>=0 and py>=0) then
		nvgLineTo(r,r);
		nvgLineTo(-r,r);
		nvgLineTo(-r,-r);
		nvgClosePath();
	else
		nvgLineTo(r,-r);
		nvgLineTo(r,r);
		nvgLineTo(-r,r);
		nvgLineTo(-r,-r);
		nvgClosePath();
	end
	nvgFillRadialGradient(0, 0, 0, 6*math.pi, Color(c,c,c,96), Color(32,32,32,32))
	nvgFill();

	-- default border colour
	local bc = Color(0,0,0,48);
	-- modify when hovering
	bc.r = lerp(bc.r, UI_HOVER_BORDER_COLOR.r, t.hoverAmount);
	bc.g = lerp(bc.g, UI_HOVER_BORDER_COLOR.g, t.hoverAmount);
	bc.b = lerp(bc.b, UI_HOVER_BORDER_COLOR.b, t.hoverAmount);
	bc.a = lerp(bc.a, UI_HOVER_BORDER_COLOR.a, t.hoverAmount);
	-- border
	nvgBeginPath();
	nvgCircle(0, 0, r);
	nvgStrokeColor(bc);
	nvgStroke();

	nvgTranslate(-cx,-cy);


	local l = nil;
	if (t.leftDown or t.leftHeld) and t.mouseInside then

		local rmx = t.mousex - cx;
		local rmy = t.mousey - cy;

		local dot = rmy*(-r);
		local l = dot/(twoPointLen(0,0,0,-r)*twoPointLen(0,0,rmx,rmy));
		l = math.acos(l);
		if (rmx<0) then 
			l = math.pi*2-l;
		end
		l = clampTo2Decimal(l/math.pi*180);
		if bSnapAngle == true then
			l = snap(l, snapAngle, min, max);
		end
		return l;

	elseif (t.mouseInside and t.mouseWheel ~= 0) then
		local step;
		if bSnapAngle then
			step = snapAngle;
		else 
			step = 1;
		end
		local op = 0;
		if t.mouseWheel < 0 then op = -1 else op = 1; end
		local ret = value + op*step;
		ret = snap(ret, step, min, max);
		return ret;
	end

	nvgRestore();
	return value;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

local function uiEditBoxExpand(text, x, y, w, anchor, expandWidth,optionalId, enabled)
	-- if editBoxData.opened == nil then
		-- editBoxData.opened = false;
	-- end

    local s = w;
    -- if editBoxData.opened then
    	-- s = expandWidth;
    -- end
    t = inputGrabRegion(x, y, s, 35, text, optionalId);
    local ret = 0;
    if t.mouseInside then
    	-- editBoxData.opened = true;
        local ex = x; -- west anchor by default
        local ew = expandWidth
        if ew == 'a' then
			ew = nvgTextWidth(text) + 20;
			if ew < w then
				ew = w;
			end
		end
		        -- w = west, e = east
        if anchor == 'e' then
            ex = x - (ew - w);
        end
        ret = uiEditBox(text, ex, y, ew, optionalId, enabled);
    else
    	-- editBoxData.opened = false;
        ret = uiEditBox(text, x, y, w, optionalId, enabled);
    end

 --    if ret == text then
	    -- m = mouseRegion(x, y, s, 35, optionalId);
	--     if m.mouseWheel ~= 0 then
	-- 	    consolePrint(m.mouseWheel);
	-- 	    local op;
	-- 	    if m.mouseWheel < 0 then op = -1 else op = 1; end
	-- 	    local step = 10;
	--     	ret = (tonumber(text)+ op * step);
	--     end
	-- end
    return ret;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

local function uiRadialEditBox(value, x, y, padx, radSize, boxSize, anchor, bSnapAngle, snapAngle, min, max, optionalId, enabled)
	local newValue = value;
	newValue = tonumber(uiEditBoxExpand(value, x + radSize + padx, y, boxSize, anchor, boxSize * 1.5, optionalId, enabled));

	bSnapAngle = bSnapAngle or false;
	snapAngle = snapAngle or consoleGetVariable("me_snapangle");
	radValue = round(tonumber(uiRadialControl(newValue, x, y, radSize, bSnapAngle, snapAngle, min, max, optionalId, enabled)));
	if radValue ~= newValue then
		return radValue;
	else
		return radValue;
	end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

function ui2Spinner_SAFE(options, selection, x, y, w, optargs)
	local optargs = optargs or {};
	local optionalId = optargs.optionalId or 0;
	local enabled = optargs.enabled == nil and true or optargs.enabled;
	local intensity = optargs.intensity or 1;
		
	local h = 35;

	-- find selected key
	local selectionKey;
	-- local lowerKey;
	-- local higherKey;
	local higherValue = nil;
	local lowerValue = nil;

	for k,v in pairs(options) do
		if v == selection then
			selectionKey = k;
		end
		if (lowerValue == nil or v > lowerValue) and v < selection then
			lowerValue = v;
		end
		if (higherValue == nil or v < higherValue) and v > selection then
			higherValue = v;
		end
	end
	-- if selectionKey == nil then
	-- 	return selection;
	-- end

	-- can we go left/right
	local canGoLeft = selectionKey and options[selectionKey-1] ~= nil or lowerValue ~= nil;
	local canGoRight = selectionKey and options[selectionKey+1] ~= nil or higherValue ~= nil;
		
	-- left
	local buttonOptargs = {};
	buttonOptargs.intensity = intensity;
	buttonOptargs.bgcoltype = UI2_COLTYPE_HOVERBUTTON;
	buttonOptargs.enabled = enabled and canGoLeft;
	if ui2Button("<", x, y, h, h, buttonOptargs) then
		if selectionKey == nil then
			-- selection = options[lowerKey];
			selection = lowerValue;
		else
			selection = options[selectionKey-1];
		end
	end;

	-- right
	buttonOptargs.enabled = enabled and canGoRight;
	if ui2Button(">", x+w-h, y, h, h, buttonOptargs) then
		if selectionKey == nil then
			-- selection = options[higherKey];
			selection = higherValue;
		else
			selection = options[selectionKey+1];
		end
	end
	
	-- consolePrint(lowerValue);
	-- label in middle
	local labelOptargs = {};
	labelOptargs.intensity = intensity;
	labelOptargs.enabled = enabled;
	labelOptargs.halign = NVG_ALIGN_CENTER;
	ui2Label(selection, x+w/2, y, labelOptargs);

	return selection;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

local function drawAngleSnap(x, y, cx, cy, bSnapAngleControl, screenSize)
	-- angle snap
	-- user = self.userData;
	x = x + uiLabel("Snap Angle: ", x, y) + 35;
	cx = cx or x;
	cy = cy or y;
	bSnapAngleControl = uiCheckBoxTooltip(cx - 30, y, 'If ON, snap control of snapping to 15 degrees.\nsnappity zippity zip snap', '', bSnapAngleControl, uniqueId, true, 'e', screenSize);
	local snapAngle = consoleGetVariable("me_snapangle");
	-- local snapAngleNew = uiEditBox(snapAngle, x, y, 60);
	-- uiRadialEditBox(value, x, y, padx, radSize, boxSize, optionalId, enabled)
	local snapAngleNew = tonumber(uiRadialEditBox(snapAngle, cx, cy, 5, 35, 40, 'w', bSnapAngleControl, 15, 0, 360));

	if snapAngleNew ~= snapAngle then
		snapAngle = snapAngleNew;
	end
	local options = {[0] = 5, [1] = 10, [2] = 15, [3] = 30, [4] = 45, [5] = 60, [6] = 90};
	local optargs = {
		intensity = 1;
	};

	snapAngleNew = ui2Spinner_SAFE(options, snapAngle, cx + 35 + 5 + 35 + 5 + 3 + 15, cy, 120, optargs);
	
	if snapAngleNew ~= snapAngle then
		snapAngle = snapAngleNew;
	end
	consolePerformCommand("me_snapangle " .. snapAngle);
	-- saveUserData(user);
	-- ui2CheckBox("5", cx + 35 + 5 + 35 + 5 + 3, cy, optargs);
	return bSnapAngleControl;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

function MapEditor:drawProperties(entityList, count)
	-- feeding in entityList, we already made sure its of the same type!
	local entity = entityList[1];
	-- 0,0 is center of screen
	count = count or 1;
	local w = 400;
	local h = viewport.height-51;
	local x = -viewport.width/2;
	local y = -viewport.height/2 + 51;
	local iy = y;
	local ix = x + 10;
	local xright = x + 150;
	local pady = 5;

    local user = self.userData;
	
	uiWindow(entity.type, x, y, w, h);
	iy = iy + UI_WINDOW_HEADER_HEIGHT;

	local propertyMaterialNames = {};
	local propertyMaterialAlbedos = {};
	local propertyPointLight = {};
	local propertySpotLight = {};
	local spotLightCombo = {};
	local pointLightCombo = {};

	-- Properties
	uiHeader("Properties", ix, iy, w);
	iy = iy + 40;

	-- EntityVolumeSelect
	if (entity.type == "VolumeSelect") then
		if uiButton("Apply Selection", nil, ix, iy, 180, UI_DEFAULT_BUTTON_HEIGHT) then
			consolePerformCommand("me_volumeselect_apply");
		end
	end

	local forceOffset = 0;

	local handledWorldSpawn = false;
	for kp, property in pairs(entity.properties) do
		
		local uniqueId = kp;
			 
		local handledName = false;
		local handledValue = false;
		local propertyHeight = 35;

		local index = string.match(property.name, "material(%d)Name");
		if (handledValue == false) and (index ~= nil) then
			handledName = true;
			handledValue = true;
			propertyHeight = -pady;
			propertyMaterialNames[tonumber(index)] = property;
		end

		index = string.match(property.name, "material(%d)Albedo");
		if (handledValue == false) and (index ~= nil) then
			handledName = true;
			handledValue = true;
			propertyHeight = -pady;
			propertyMaterialAlbedos[tonumber(index)] = property;
		end

		if (handledValue == false) then
			local pointLightProperty = string.match(property.name, "pointLight.-$");
			if (pointLightProperty ~= nil) then
				propertyPointLight[pointLightProperty] = {};
				propertyPointLight[pointLightProperty].value = property.value;
				propertyPointLight[pointLightProperty].baseValue = property.baseValue;
				propertyHeight = -pady;
				handledValue = true;
				handledName = true;
			end
		end

		if (handledValue == false) then
			local spotLightProperty = string.match(property.name, "spotLight.-$");
			if (spotLightProperty ~= nil) then
				propertySpotLight[spotLightProperty] = {};
				propertySpotLight[spotLightProperty].value = property.value;
				propertySpotLight[spotLightProperty].baseValue = property.baseValue;
				propertyHeight = -pady;
				handledValue = true;
				handledName = true;
			end
		end

		-- if (entity.type == "WorldSpawn" and handledWorldSpawn == false) then
		-- 	handledWorldSpawn = true;
		-- 	local xo = xright;
		-- 	local boxsize = 242/3-3;
		-- 	local trim = 5;
		-- 	local radialsize = 35;
		-- 	local anchor = 'w'; -- use 'e' or 'w'
		-- 	user.bSnapAngle = uiCheckBoxTooltip(xo-30, iy, 'If ON, angle rotation will be snapped according to Snap Angle.', '', user.bSnapAngle, uniqueId, true, 'e');

		-- 	-- angles
	 --       	-- uiRadialEditBox(value, x, y, padx, radSize, boxSize, optionalId, enabled)
	 --       	local snapAngle = consoleGetVariable("me_snapangle");
	 --       	local min = 0;
	 --       	local max = 360;
		-- 	p.x = tonumber(uiRadialEditBox(property.value.x, xo, iy, trim, radialsize, boxsize - radialsize - trim, anchor, user.bSnapAngle, snapAngle, min, max, uniqueId, true));
		-- 	p.y = tonumber(uiRadialEditBox(property.value.y, xo + boxsize + trim, iy, trim, radialsize, boxsize - radialsize - trim, anchor, user.bSnapAngle, snapAngle, min, max, uniqueId, true));
		-- 	p.z = tonumber(uiRadialEditBox(property.value.z, xo + boxsize + boxsize + trim + trim, iy, trim, radialsize, boxsize - radialsize - trim, anchor, user.bSnapAngle, snapAngle, min, max, uniqueId, true));
		-- 	-- p.x = uiRadialControl(property.value.x, xright, iy, radialsize, uniqueId)
		-- 	-- if user.bSnapAngle == true then
		-- 	-- 	user.bSnapAngleControl = drawAngleSnap(ix, iy + propertyHeight + pady, xright, iy + propertyHeight + pady, user.bSnapAngleControl);
		-- 	-- 	forceOffset = propertyHeight + pady;
		-- 	-- end
		-- end
		-- EntityEffect.effectName
		if (handledValue == false) and (property.type == "string") and (entity.type == "Effect") and (property.name == "effectName") then
			local function commandToApplyEffect(id) return "me_setentityproperty " .. id .. " " .. property.name .. " %s"; end

			-- thumb image
			local thumb = "$thumb_mesh/" .. property.value;
			if uiThumbnail(xright, iy, thumbSize, thumbSize, thumb, uniqueId) then
				-- spawn content browser
				showAsPopup("ContentBrowser", "effect", property.value, commandToApplyEffect(entity.id));
			end

			-- editbox
										-- (text, x, y, w, anchor, expandWidth,optionalId, enabled)
			local valueNew = uiEditBoxExpand(property.value, xright + thumbSize + 5, iy, 167, 'w', 'a', uniqueId);
			if valueNew ~= property.value then
				for k, e in pairs(entityList) do
					consolePerformCommand(string.format(commandToApplyEffect(e.id), valueNew));
				end
			end
			
			handledValue = true;
			propertyHeight = thumbSize;
		end

		-- EntityPickup.type
		if (handledValue == false) and (property.type == "number") and (entity.type == "Pickup") and (property.name == "pickupType") then
			local p = math.floor(tonumber(uiComboBoxPickup(property.value, xright, iy, 200, self.pickupComboBoxData, uniqueId)));
			if p ~= property.value then
				for k, e in pairs(entityList) do
					consolePerformCommand("me_setentityproperty " .. e.id .. " " .. property.name .. " " .. p);
				end
			end
			handledValue = true;
		end

		-- vec3
		if (handledValue == false) and property.type == "vec3" then
			local p = {};
			local xo = xright;
			local boxsize = 242/3-3;
			local trim = 5;
			local radialsize = 35;
			local anchor = 'w'; -- use 'e' or 'w'
			if (property.name == "angles") then
		        -- tooltip
				-- uiCheckBoxTooltip(x, y, toolTipText, label, value, optionalId, enabled)
				user.bSnapAngle = uiCheckBoxTooltip(xo-30, iy, 'If ON, angle rotation will be snapped according to Snap Angle.', '', user.bSnapAngle, uniqueId, true, 'e');

				-- angles
		       	-- uiRadialEditBox(value, x, y, padx, radSize, boxSize, optionalId, enabled)
		       	local snapAngle = consoleGetVariable("me_snapangle");
		       	local min = 0;
		       	local max = 360;
				p.x = tonumber(uiRadialEditBox(property.value.x, xo, iy, trim, radialsize, boxsize - radialsize - trim, anchor, user.bSnapAngle, snapAngle, min, max, uniqueId, true));
				p.y = tonumber(uiRadialEditBox(property.value.y, xo + boxsize + trim, iy, trim, radialsize, boxsize - radialsize - trim, anchor, user.bSnapAngle, snapAngle, min, max, uniqueId, true));
				p.z = tonumber(uiRadialEditBox(property.value.z, xo + boxsize + boxsize + trim + trim, iy, trim, radialsize, boxsize - radialsize - trim, anchor, user.bSnapAngle, snapAngle, min, max, uniqueId, true));
				-- p.x = uiRadialControl(property.value.x, xright, iy, radialsize, uniqueId)
				if user.bSnapAngle == true then
					user.bSnapAngleControl = drawAngleSnap(ix, iy + propertyHeight + pady, xright, iy + propertyHeight + pady, user.bSnapAngleControl);
					forceOffset = propertyHeight + pady;
				end
			else
				-- position
				p.x = tonumber(uiEditBox(property.value.x, xright, iy, boxsize, uniqueId));
				p.y = tonumber(uiEditBox(property.value.y, xright+boxsize+5, iy, boxsize, uniqueId));
				p.z = tonumber(uiEditBox(property.value.z, xright+2*(boxsize+5), iy, boxsize, uniqueId));
			end
			if (p.x ~= nil) and (p.y ~= nil) and (p.z ~= nil) and (p.x ~= property.value.x or p.y ~= property.value.y or p.z ~= property.value.z) then
				-- consolePrint(table.getn(entityList));
				for k, e in pairs(entityList) do
					consolePerformCommand("me_setentityproperty " .. e.id .. " " .. property.name .. " " .. p.x .. " " .. p.y .. " " .. p.z);
				end
			end

			handledValue = true;
		end

		-- bool
		if (handledValue == false) and property.type == "bool" then
			local newValue = uiCheckBox(property.value, property.name, x+18, iy, uniqueId);
			if newValue ~= property.value then
				for k, e in pairs(entityList) do
					consolePerformCommand("me_setentityproperty " .. e.id .. " " .. property.name .. " " .. (newValue and 1 or 0));
				end
			end

			handledValue = true;
			handledName = true;
		end

		-- string
		if (handledValue == false) and (property.type == "string" or property.type == "number") then
			local p;
			local boxsize = 242/3-3;
			local trim = 5;
			local radialsize = 35;
			if (property.name == 'timeOfDay') then
				-- user.bSnapAngle = uiCheckBoxTooltip(xright-30, iy, 'If ON, angle rotation will be snapped according to Snap Angle.', '', user.bSnapAngle, uniqueId, true, 'e');
				p = tonumber(uiRadialEditBox(property.value, xright, iy, trim, radialsize, boxsize - radialsize - trim, 'w', user.bSnapAngle, 1, 0, 24, uniqueId, true));
			elseif (property.name == 'skyAngle') then
		       	local snapAngle = consoleGetVariable("me_snapangle");
				user.bSnapAngle = uiCheckBoxTooltip(xright-30, iy, 'If ON, angle rotation will be snapped according to Snap Angle.', '', user.bSnapAngle, uniqueId, true, 'e');
				p = tonumber(uiRadialEditBox(property.value, xright, iy, trim, radialsize, boxsize - radialsize - trim, 'w', user.bSnapAngle, snapAngle, 0, 360, uniqueId, true));
				if user.bSnapAngle == true then
					user.bSnapAngleControl = drawAngleSnap(ix, iy + propertyHeight + pady, xright, iy + propertyHeight + pady, user.bSnapAngleControl);
					forceOffset = propertyHeight + pady;
				end
			else
				p = uiEditBox(property.value, xright, iy, 242, uniqueId);
			end
			if tostring(p) ~= tostring(property.value) then
				for k, e in pairs(entityList) do
					consolePerformCommand("me_setentityproperty " .. e.id .. " " .. property.name .. " " .. p);
				end
			end

			handledValue = true;
		end

		-- colour
		if (handledValue == false) and (property.type == "colorXrgb") then
			function commandToApplyColor(id)
				return "me_setentityproperty " .. id .. " " .. property.name .. " %s";
			end
			-- local commandToApplyColor = "me_setentityproperty " .. entity.id .. " " .. property.name .. " %s";
			local hexCol = rgbToHex(property.value);
				
			-- thumbnail
			if uiThumbnailColor(xright+1, iy+3, 28, 28, property.value, uniqueId) then
				showAsPopup("ColorPicker", "ff" .. hexCol, "noalpha", commandToApplyColor(entity.id));
			end

			-- editbox
			local hexColNew = uiEditBox(hexCol, xright+35, iy, 75, uniqueId);
			if hexCol ~= hexColNew then
				local argb = "ff" .. hexColNew;
				for k, e in pairs(entityList) do
					consolePerformCommand(string.format(commandToApplyColor(e.id), argb));
				end
			end
		end

		if not handledName then
			local propertyName = property.name;
			local propertyLen = string.len(propertyName);
			if propertyLen > 12 then
				propertyName = string.sub(propertyName, 0, 12) .. "..";
			end
			propertyLen = math.min(propertyLen, 15)
			uiLabel(propertyName, ix+10, iy);
		end

		iy = iy + propertyHeight + pady;
		iy = iy + forceOffset;
		forceOffset = 0;
	end

	-- Light Properties
	local hasPointLight = (propertyPointLight["pointLightIntensity"] ~= nil) and (propertyPointLight["pointLightIntensity"].baseValue ~= nil);
	local hasSpotLight = (propertySpotLight["spotLightIntensity"] ~= nil) and (propertySpotLight["spotLightIntensity"].baseValue ~= nil);
	if hasPointLight or hasSpotLight then
		uiHeader("Light Properties", ix, iy, w);
		iy = iy + 40;
	end
	
	-- pointlight
	if hasPointLight then
		local lightApplied = propertyPointLight["pointLightOverridden"].value == true;
				

		-- local commandToApplyOverridden = "me_setentityproperty " .. entity.id .. " pointLightOverridden %s";
		-- local commandToApplyIntensity = "me_setentityproperty " .. entity.id .. " pointLightIntensity %s";
		-- local commandToApplyTeamIndex = "me_setentityproperty " .. entity.id .. " pointLightTeamIndex %s";
		-- local commandToApplyColor = "me_setentityproperty " .. entity.id .. " pointLightColor %s";
		-- local commandToApplyNear = "me_setentityproperty " .. entity.id .. " pointLightNear %s";
		-- local commandToApplyFar = "me_setentityproperty " .. entity.id .. " pointLightFar %s";
		local function commandToApplyOverriddenPoint(id) return "me_setentityproperty " .. id .. " pointLightOverridden %s"; end
		local function commandToApplyIntensityPoint(id) return "me_setentityproperty " .. id .. " pointLightIntensity %s"; end
		local function commandToApplyTeamIndexPoint(id) return "me_setentityproperty " .. id .. " pointLightTeamIndex %s"; end
		local function commandToApplyColorPoint(id) return "me_setentityproperty " .. id .. " pointLightColor %s"; end
		local function commandToApplyNearPoint(id) return "me_setentityproperty " .. id .. " pointLightNear %s"; end
		local function commandToApplyFarPoint(id) return "me_setentityproperty " .. id .. " pointLightFar %s"; end
		
		local uniqueId = 0;
		local lightAppliedNew = uiCheckBox(lightApplied, "pointlight", ix+10, iy, uniqueId);
		if lightAppliedNew ~= lightApplied then
			if lightAppliedNew then
				-- set a material (start with base)
				for k, e in pairs(entityList) do
					consolePerformCommand(string.format(commandToApplyOverriddenPoint(e.id), "1"));
					consolePerformCommand(string.format(commandToApplyIntensityPoint(e.id), propertyPointLight["pointLightIntensity"].baseValue));
					consolePerformCommand(string.format(commandToApplyTeamIndexPoint(e.id), propertyPointLight["pointLightTeamIndex"].baseValue));
					consolePerformCommand(string.format(commandToApplyColorPoint(e.id), rgbToHex(propertyPointLight["pointLightColor"].baseValue)));
					consolePerformCommand(string.format(commandToApplyNearPoint(e.id), propertyPointLight["pointLightNear"].baseValue));
					consolePerformCommand(string.format(commandToApplyFarPoint(e.id), propertyPointLight["pointLightFar"].baseValue));
				end
			else
				-- remove the material (clearing all properties to defaults)
				for k, e in pairs(entityList) do
					consolePerformCommand(string.format(commandToApplyOverriddenPoint(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyIntensityPoint(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyTeamIndexPoint(e.id), "-1"));
					consolePerformCommand(string.format(commandToApplyColorPoint(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyNearPoint(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyFarPoint(e.id), "0"));
				end
			end
		end
	
		-- col
		uiLabel("col", xright, iy, lightApplied);
		local lightColor = lightApplied and propertyPointLight["pointLightColor"].value or propertyPointLight["pointLightColor"].baseValue;
		if uiThumbnailColor(xright+40, iy+4, 28, 28, lightColor, uniqueId, lightApplied) then
			-- spawn content browser
			local hexCol = rgbToHex(lightColor);
			showAsPopup("ColorPicker", "ff" .. hexCol, "noalpha", commandToApplyColorPoint(entity.id));
		end
		
		-- editbox
		local hexCol = rgbToHex(lightColor);
		local hexColNew = uiEditBox(hexCol, xright + 73, iy, 75, uniqueId, lightApplied);
		if hexCol ~= hexColNew then
			local argb = "ff" .. hexColNew;
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyColorPoint(e.id), argb));
			end
		end
		
		-- intensity
		local intensity = clampTo2Decimal(lightApplied and propertyPointLight["pointLightIntensity"].value or propertyPointLight["pointLightIntensity"].baseValue);
		uiLabel("x", xright+160, iy, lightApplied);
		local intensityNew = clampTo2Decimal(uiEditBox(intensity, xright+182, iy, 60, 0, lightApplied));
		if intensity ~= intensityNew then
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyIntensityPoint(e.id), intensityNew));
			end
		end

		-- near
		local near = clampTo2Decimal(lightApplied and propertyPointLight["pointLightNear"].value or propertyPointLight["pointLightNear"].baseValue);
		uiLabel("near ", xright, iy+40, lightApplied);
		local nearNew = uiEditBox2Decimals(near, xright+73, iy+40, 60, 0, lightApplied);
		if near ~= nearNew then
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyNearPoint(e.id), nearNew));
			end
		end

		-- far
		local far = clampTo2Decimal(lightApplied and propertyPointLight["pointLightFar"].value or propertyPointLight["pointLightFar"].baseValue);
		uiLabel("far ", xright+153, iy+40, lightApplied);
		local farNew = uiEditBox2Decimals(far, xright+182, iy+40, 60, 0, lightApplied);
		if far ~= farNew then
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyFarPoint(e.id), farNew));
			end
		end
	
		-- teamIndex
		local teamIndex = tonumber(lightApplied and propertyPointLight["pointLightTeamIndex"].value or propertyPointLight["pointLightTeamIndex"].baseValue);
		uiLabel("team", xright, iy+80, lightApplied);
		pointLightCombo.x = xright+73;
		pointLightCombo.y = iy+80;
		pointLightCombo.value = teamIndex;
		pointLightCombo.enabled = lightApplied;
		-- pointLightCombo.command = function(id) return "me_setentityproperty " .. id .. " pointLightTeamIndex %s"; end
		pointLightCombo.command = commandToApplyTeamIndexPoint;

		iy = iy + 120;
	end
	
	-- spotlight
	if hasSpotLight then
		local lightApplied = propertySpotLight["spotLightOverridden"].value == true;
				
		local function commandToApplyTeamIndex(id) return "me_setentityproperty " .. id .. " spotLightTeamIndex %s"; end
		local function commandToApplyInnerAnglesDegrees(id) return "me_setentityproperty " .. id .. " spotLightInnerAnglesDegrees %s"; end
		local function commandToApplyOuterAnglesDegrees(id) return "me_setentityproperty " .. id .. " spotLightOuterAnglesDegrees %s"; end
		local function commandToApplyCastsShadow(id) return "me_setentityproperty " .. id .. " spotLightCastsShadow %s"; end
		local function commandToApplyOverridden(id) return "me_setentityproperty " .. id .. " spotLightOverridden %s"; end
		local function commandToApplyIntensity(id) return "me_setentityproperty " .. id .. " spotLightIntensity %s"; end
		local function commandToApplyColor(id) return "me_setentityproperty " .. id .. " spotLightColor %s"; end
		local function commandToApplyNear(id) return "me_setentityproperty " .. id .. " spotLightNear %s"; end
		local function commandToApplyFar(id) return "me_setentityproperty " .. id .. " spotLightFar %s"; end

		local uniqueId = 0;
		local lightAppliedNew = uiCheckBox(lightApplied, "spotlight", ix+10, iy, uniqueId);
		if lightAppliedNew ~= lightApplied then
			if lightAppliedNew then
				-- set a material (start with base)
				for k, e in pairs(entityList) do
					consolePerformCommand(string.format(commandToApplyOverridden(e.id), "1"));
					consolePerformCommand(string.format(commandToApplyInnerAnglesDegrees(e.id), propertySpotLight["spotLightInnerAnglesDegrees"].baseValue));
					consolePerformCommand(string.format(commandToApplyOuterAnglesDegrees(e.id), propertySpotLight["spotLightOuterAnglesDegrees"].baseValue));
					consolePerformCommand(string.format(commandToApplyCastsShadow(e.id), propertySpotLight["spotLightCastsShadow"].baseValue and "1" or "0"));
					consolePerformCommand(string.format(commandToApplyTeamIndex(e.id), propertySpotLight["spotLightTeamIndex"].baseValue));
					consolePerformCommand(string.format(commandToApplyIntensity(e.id), propertySpotLight["spotLightIntensity"].baseValue));
					consolePerformCommand(string.format(commandToApplyColor(e.id), rgbToHex(propertySpotLight["spotLightColor"].baseValue)));
					consolePerformCommand(string.format(commandToApplyNear(e.id), propertySpotLight["spotLightNear"].baseValue));
					consolePerformCommand(string.format(commandToApplyFar(e.id), propertySpotLight["spotLightFar"].baseValue));
				end
			else
				-- remove the material (clearing all properties to defaults)
				for k, e in pairs(entityList) do
					consolePerformCommand(string.format(commandToApplyInnerAnglesDegrees(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyOuterAnglesDegrees(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyCastsShadow(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyOverridden(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyTeamIndex(e.id), "-1"));
					consolePerformCommand(string.format(commandToApplyIntensity(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyColor(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyNear(e.id), "0"));
					consolePerformCommand(string.format(commandToApplyFar(e.id), "0"));
				end
			end
		end
	
		-- col
		uiLabel("col", xright, iy, lightApplied);
		local lightColor = lightApplied and propertySpotLight["spotLightColor"].value or propertySpotLight["spotLightColor"].baseValue;
		if uiThumbnailColor(xright+40, iy+4, 28, 28, lightColor, uniqueId, lightApplied) then
			-- spawn content browser
			local hexCol = rgbToHex(lightColor);
			-- for k, e in pairs(entityList) do
				showAsPopup("ColorPicker", "ff" .. hexCol, "noalpha", commandToApplyColor(entity.id));
			-- end
		end
		
		-- editbox
		local hexCol = rgbToHex(lightColor);
		local hexColNew = uiEditBox(hexCol, xright + 73, iy, 75, uniqueId, lightApplied);
		if hexCol ~= hexColNew then
			local argb = "ff" .. hexColNew;
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyColor(e.id), argb));
			end
		end
		
		-- intensity
		local intensity = clampTo2Decimal(lightApplied and propertySpotLight["spotLightIntensity"].value or propertySpotLight["spotLightIntensity"].baseValue);
		uiLabel("x", xright+160, iy, lightApplied);
		local intensityNew = clampTo2Decimal(uiEditBox(intensity, xright+182, iy, 60, 0, lightApplied));
		if intensity ~= intensityNew then
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyIntensity(e.id), intensityNew));
			end
		end

		-- near
		local near = clampTo2Decimal(lightApplied and propertySpotLight["spotLightNear"].value or propertySpotLight["spotLightNear"].baseValue);
		uiLabel("near", xright, iy+40, lightApplied);
		local nearNew = uiEditBox2Decimals(near, xright+73, iy+40, 60, 0, lightApplied);
		if near ~= nearNew then
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyNear(e.id), nearNew));
			end
		end

		-- far
		local far = clampTo2Decimal(lightApplied and propertySpotLight["spotLightFar"].value or propertySpotLight["spotLightFar"].baseValue);
		uiLabel("far", xright+153, iy+40, lightApplied);
		local farNew = uiEditBox2Decimals(far, xright+182, iy+40, 60, 0, lightApplied);
		if far ~= farNew then
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyFar(e.id), farNew));
			end
		end

		-- inner
		local inner = clampTo2Decimal(lightApplied and propertySpotLight["spotLightInnerAnglesDegrees"].value or propertySpotLight["spotLightInnerAnglesDegrees"].baseValue);
		uiLabel("inner", xright, iy+80, lightApplied);
		local innerNew = uiEditBox2Decimals(inner, xright+73, iy+80, 60, 0, lightApplied);
		if inner ~= innerNew then
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyInnerAnglesDegrees(e.id), innerNew));
			end
		end

		-- outer
		local outer = clampTo2Decimal(lightApplied and propertySpotLight["spotLightOuterAnglesDegrees"].value or propertySpotLight["spotLightOuterAnglesDegrees"].baseValue);
		uiLabel("outer", xright+137, iy+80, lightApplied);
		local outerNew = uiEditBox2Decimals(outer, xright+182, iy+80, 60, 0, lightApplied);
		if outer ~= outerNew then
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyOuterAnglesDegrees(e.id), outerNew));
			end
		end
	
		-- teamIndex
		local teamIndex = tonumber(lightApplied and propertySpotLight["spotLightTeamIndex"].value or propertySpotLight["spotLightTeamIndex"].baseValue);
		uiLabel("team", xright, iy+120, lightApplied);
		spotLightCombo.x = xright+73;
		spotLightCombo.y = iy+120;
		spotLightCombo.value = teamIndex;
		spotLightCombo.enabled = lightApplied;
		spotLightCombo.command = commandToApplyTeamIndex;

		-- shadow?
		local castsShadow = lightApplied and (propertySpotLight["spotLightCastsShadow"].value and 1 or 0) or (propertySpotLight["spotLightCastsShadow"].baseValue and 1 or 0);
		castsShadow = (castsShadow == 1) and true or false;
		local castsShadowNew = uiCheckBox(castsShadow, "shadow", xright+150, iy+120, uniqueId, lightApplied);
		if castsShadow ~= castsShadowNew then
			castsShadowNew = castsShadowNew and 1 or 0;
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(commandToApplyCastsShadow(e.id), tostring(castsShadowNew)));
			end
		end

		iy = iy + 160;
	end

	-- Materials Properties
	if propertyMaterialNames[0] ~= nil and propertyMaterialNames[0].baseValue ~= nil then
		uiHeader("Material Properties", ix, iy, w);
		iy = iy + 40;
	end
	for index = 0, 7 do
		local propertyMaterialName = propertyMaterialNames[index];
		local propertyMaterialAlbedo = propertyMaterialAlbedos[index];

		if propertyMaterialName ~= nil and propertyMaterialAlbedo ~= nil and
			propertyMaterialName.baseValue ~= nil and propertyMaterialAlbedo.baseValue ~= nil then

			local uniqueId = index;
			
			local materialApplied = true;
			local effectiveMaterial = propertyMaterialName.value;
			if string.len(effectiveMaterial) <= 0 then
				effectiveMaterial = propertyMaterialName.baseValue;
				materialApplied = false;
			end

			local albedoApplied = true;
			local effectiveAlbedo = propertyMaterialAlbedo.value;
			if effectiveAlbedo.a <= 0 then
				effectiveAlbedo = propertyMaterialAlbedo.baseValue;
				albedoApplied = false;
			end

			-- material
			local function commandToApplyMaterial(id) return "me_setentityproperty " .. id .. " " .. propertyMaterialName.name .. " %s"; end

			local materialAppliedNew = uiCheckBox(materialApplied, "material "..index, ix+10, iy, uniqueId);
			if materialAppliedNew ~= materialApplied then
				if materialAppliedNew then
					-- set a material (start with base)
					for k, e in pairs(entityList) do
						consolePerformCommand(string.format(commandToApplyMaterial(e.id), propertyMaterialName.baseValue));
					end
				else
					-- remove the material
					for k, e in pairs(entityList) do
						consolePerformCommand(string.format(commandToApplyMaterial(e.id), ""));
					end
				end
			end
			
			-- thumb image
			local thumb = "$thumb_material/" .. effectiveMaterial;
			if uiThumbnail(xright, iy, thumbSize, thumbSize, thumb, uniqueId, materialApplied) then
				-- spawn content browser
				showAsPopup("ContentBrowser", "material", materialApplied, commandToApplyMaterial(entity.id));
			end

			-- editboxt
										-- (text, x, y, w, anchor, expandWidth,optionalId, enabled)
			effectiveMaterialNew = uiEditBoxExpand(effectiveMaterial, xright + thumbSize + 5, iy-1, 167, 'w', 'a', uniqueId, materialApplied);
			if effectiveMaterialNew ~= effectiveMaterial then
				-- set material
				for k, e in pairs(entityList) do
					consolePerformCommand("me_setentityproperty " .. e.id .. " " .. propertyMaterialName.name .. " " .. effectiveMaterialNew);
				end
					
			end

			-- user color option
			local albedoAppliedNew = uiCheckBox(albedoApplied, "col", xright + 76, iy+34, uniqueId, materialSupportsAlbedo);
			if albedoAppliedNew ~= albedoApplied then
				if albedoAppliedNew then
					-- set the albedo (start with red)
					for k, e in pairs(entityList) do
						consolePerformCommand("me_setentityproperty " .. e.id .. " " .. propertyMaterialAlbedo.name .. " " .. argbToHex(propertyMaterialAlbedo.baseValue));
					end
						
				else
					-- remove the albedo
					for k, e in pairs(entityList) do
						consolePerformCommand("me_setentityproperty " .. e.id .. " " .. propertyMaterialAlbedo.name .. " 0 0 0 0");
					end
						
				end
			end
			
			---- editbox
			local function commandToApplyColor(id) return "me_setentityproperty " .. id .. " " .. propertyMaterialAlbedo.name .. " %s"; end

			local hexCol = rgbToHex(effectiveAlbedo);
			local hexColNew = uiEditBox(hexCol, xright + 167, iy+35, 75, uniqueId, albedoApplied);
			if hexCol ~= hexColNew then
				local argb = "ff" .. hexColNew;
				for k, e in pairs(entityList) do
					consolePerformCommand(string.format(commandToApplyColor(e.id), argb));
				end
			end

			-- thumb color
			if effectiveAlbedo.a > 0 then
				if uiThumbnailColor(xright+134, iy+35+3, 28, 28, effectiveAlbedo, uniqueId, albedoApplied) then
					showAsPopup("ColorPicker", "ff" .. hexCol, "noalpha", commandToApplyColor(entity.id));
				end
			end
			iy = iy + thumbSize + pady;
			
		end
	end

	-- combos at end
	if spotLightCombo.x ~= nil then
		local teamIndex = spotLightCombo.value;
		local teamIndexNew = uiTeamIndexCombo(teamIndex, spotLightCombo.x, spotLightCombo.y, 60, self.spotlightTeamIndexComboBoxData, 0, spotLightCombo.enabled);
		if teamIndex ~= teamIndexNew then
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(spotLightCombo.command(e.id), teamIndexNew));
			end
		end
	end
	if pointLightCombo.x ~= nil then
		local teamIndex = pointLightCombo.value;
		local teamIndexNew = uiTeamIndexCombo(teamIndex, pointLightCombo.x, pointLightCombo.y, 60, self.pointlightTeamIndexComboBoxData, 0, pointLightCombo.enabled);
		if teamIndex ~= teamIndexNew then
			for k, e in pairs(entityList) do
				consolePerformCommand(string.format(pointLightCombo.command(e.id), teamIndexNew));
			end
		end
	end

	saveUserData(user);
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
function MapEditor:drawToolBar(canDisplayProperties)
	-- 0,0 is center of screen
	local w = viewport.width;
	local h = 50;
	local x = -viewport.width/2;
	local y = -viewport.height/2;--0; --viewport.height/2 - h;
	local iy = y + 7;
	local ix = x + 10;

	-- bg
	nvgBeginPath();
	nvgRect(x, y, w, h, 0);
	nvgFillColor(Color(34, 36, 40, 242));
	nvgFill();

	-- bottom line
	nvgBeginPath();
	nvgMoveTo(x, y+h);
	nvgLineTo(x+w, y+h);
	nvgStrokeWidth(1);
	nvgStrokeColor(Color(150, 150, 150, 80));
	nvgStroke();

	-- logo
	local logorad = 20;
	local svgName = "internal/ui/icons/reflexlogo";
	nvgFillColor(Color(255, 255, 255, 255));
	nvgSvg(svgName, ix+13, iy+17, logorad);
	ix = ix + 40;
	
	-- "Reflex"
	nvgFontSize(46);
	nvgFontFace(FONT_HEADER);
	nvgTextAlign(NVG_ALIGN_LEFT, NVG_ALIGN_BASELINE);
	nvgFontBlur(0);
	nvgFillColor(Color(255, 255, 255, 255));
	nvgText(ix, iy+30, "REFLEX");
	ix = ix + 98;
	nvgFillColor(Color(160, 50, 50, 255));
	nvgText(ix, iy+30, "DEV");
	ix = ix + 60;

	uiToolBarSeparator(ix, iy);
	ix = ix + 10;
	
	-- save icon
	if uiButton("Save", nil, ix, iy, 60, UI_DEFAULT_BUTTON_HEIGHT) then
		consolePerformCommand("savemap");
	end
	ix = ix + 70;
	uiToolBarSeparator(ix, iy);
	ix = ix + 10;

	-- properties icon
	-- show/hide entity properties
	local showProperties = consoleGetVariable("me_showproperties");
	local showPropertiesCol = nil;
	if showProperties ~= 0 then
		showPropertiesCol = UI_COLOR_RED;
	end
	if uiButton("Properties", nil, ix, iy, 110, UI_DEFAULT_BUTTON_HEIGHT, showPropertiesCol, 0, canDisplayProperties) then
		if showProperties ~= 0 then 
			consolePerformCommand("me_showproperties 0");
		else
			consolePerformCommand("me_showproperties 1");
		end
	end
	ix = ix + 120;
	uiToolBarSeparator(ix, iy);
	ix = ix + 10;

	-- me_createtype -- entity type (combobox?)
	local entityTypes = {};
	entityTypes[1] = "WorldSpawn";
	entityTypes[2] = "Effect";
	entityTypes[3] = "Pickup";
	entityTypes[4] = "Jumppad";
	entityTypes[5] = "PlayerSpawn";
	entityTypes[6] = "PointLight";
	entityTypes[7] = "RaceFinish";
	entityTypes[8] = "RaceStart";
	entityTypes[9] = "Teleporter";
	entityTypes[10] = "Target";
	entityTypes[11] = "WeaponRestrictor";
	entityTypes[12] = "Prefab";
	entityTypes[13] = "VolumeSelect";
	local entityType = consoleGetVariable("me_createtype");
	local entityTypeNew = uiComboBox(entityTypes, entityType, ix, iy, 180, self.entityTypeComboBoxData, optionalId, enabled)
	if entityTypeNew ~= entityType then
		consolePerformCommand("me_createtype " .. entityTypeNew);
	end
	ix = ix + 190;

	-- me_effecttype ?

	uiToolBarSeparator(ix, iy);
	ix = ix + 10;

	--
	-- MATERIAL
	--

	-- me_activematerial (only when editing worldspawn)
	local activeMaterial = consoleGetVariable("me_activematerial");
	local canUseMaterial = true; -- (string.lower(entityType) == string.lower(entityTypes[1])); [pb] it's nice to have this always on
	
	ix = ix + uiLabel("Material: ", ix, iy, canUseMaterial);

	-- mat editbox
							-- local function uiEditBoxExpand(text, x, y, w, anchor, expandWidth,optionalId, enabled)
	local activeMaterialNew = uiEditBox(activeMaterial, ix, iy, 260, 0, canUseMaterial);
	if activeMaterialNew ~= activeMaterial then
		consolePerformCommand("me_activematerial " .. activeMaterialNew);
	end
	ix = ix + 265;

	-- mat image
	local thumbName = "$thumb_material/" .. activeMaterial;
	if uiThumbnail(ix, iy, 35, 35, thumbName, 0, canUseMaterial) then
		-- spawn content browser
		showAsPopup("ContentBrowser", "material", activeMaterial, "me_activematerial %s");
	end
	ix = ix + 40;

	-- read col
	local hexCol = consoleGetVariable("me_activealbedo");
	local col = hexArgbToCol(hexCol);
	local colOn = col.a > 0;
	hexCol = string.sub(hexCol, 3, 8); -- remove alpha
	if not colOn then 
		hexCol = "";
		col = defaultBrushMaterialColor;
	end

	local colOnNew = uiCheckBox(colOn, "Color", ix, iy, 0, canUseMaterial);
	if colOnNew ~= colOn then
		if colOnNew then
			consolePerformCommand("me_activealbedo " .. argbToHex(defaultBrushMaterialColor));
		else
			consolePerformCommand("me_activealbedo 0");
		end
	end
	ix = ix + 77;

	-- color thumb
	if uiThumbnailColor(ix, iy+4, 28, 28, col, 0, colOn) then
		showAsPopup("ColorPicker", "ff" .. hexCol, "noalpha", "me_activealbedo %s");
	end
	ix = ix + 32;

	-- color editbox
	local hexColNew = uiEditBox(hexCol, ix, iy, 75, 0, colOn);
	if hexCol ~= hexColNew then
		local argb = "ff" .. hexColNew;
		consolePerformCommand("me_activealbedo " .. argb);
	end
	ix = ix + 85;

	uiToolBarSeparator(ix, iy);
	ix = ix + 10;
	
	-- distance snap
	ix = ix + uiLabel("Snap Distance: ", ix, iy);
	local snapDistance = consoleGetVariable("me_snapdistance");
	local snapDistanceNew = uiEditBox(snapDistance, ix, iy, 60);
	if snapDistanceNew ~= snapDistance then
		consolePerformCommand("me_snapdistance " .. snapDistanceNew);
	end
	ix = ix + 70;
	uiToolBarSeparator(ix, iy);
	ix = ix + 10;
	
	-- -- angle snap
	-- ix = ix + uiLabel("Snap Angle: ", ix, iy);
	-- local snapAngle = consoleGetVariable("me_snapangle");
	-- -- local snapAngleNew = uiEditBox(snapAngle, ix, iy, 60);
	-- -- uiRadialEditBox(value, x, y, padx, radSize, boxSize, optionalId, enabled)
	-- local snapAngleNew = tonumber(uiRadialEditBox(snapAngle, ix, iy, 5, 35, 40));
	-- if snapAngleNew ~= snapAngle then
	-- 	consolePerformCommand("me_snapangle " .. snapAngleNew);
	-- end
	local screenSize = {};
	screenSize.x = x;
	screenSize.y = y;
	screenSize.ex = x + viewport.width;
	screenSize.ey = y + viewport.height;
	self.userData.bSnapAngleControl = drawAngleSnap(ix, iy, nil, nil, self.userData.bSnapAngleControl, screenSize);
	ix = ix + 65;

	ix = ix + 10;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
function MapEditor:draw()
	-- only display when in editor mode
	local player = getLocalPlayer();
    if player == nil or player.state ~= PLAYER_STATE_EDITOR then
		return
	end

	local firstEntity = nil;
	local selectedEntityCount = 0;
	local bSameType = true;
	--delete
	bSameType = false;
	local etype = nil;
	local entityList = {}

	for ke, entity in pairs(selectedEntities) do
		-- if etype == nil then etype = entity.type; end
		selectedEntityCount = selectedEntityCount + 1;
		-- if bSameType == false then 
			-- break 
		-- end;
		-- if etype ~= entity.type then
			-- bSameType = false;
		-- end
		firstEntity = entity;
		-- etype = entity.type;
		table.insert(entityList, entity);
	end

	local canDisplayProperties = (firstEntity ~= nil and bSameType) or selectedEntityCount == 1;

	-- only display if have some selection
	if canDisplayProperties and consoleGetVariable("me_showproperties") ~= 0 then
		self:drawProperties(entityList);
	end

	if selectedEntityCount > 1 then
		-- 0,0 is center of screen
		local w = 400;
		local h = UI_WINDOW_HEADER_HEIGHT;
		local x = -viewport.width/2;
		local y = -viewport.height/2 + 51;

		local selectedBrushCount = 0;
		for kb, brush in pairs(selectedBrushes) do
			selectedBrushCount = selectedBrushCount + 1;
		end

		local headerString;
		-- if bSameType then
			-- headerString = "Selected " .. selectedEntityCount .. " " .. etype;
		-- else
			headerString = selectedEntityCount .. " entities, " .. selectedBrushCount .. " brushes selected";
		-- end
		uiWindow(headerString, x, y, w, h);
	end

	self:drawToolBar(canDisplayProperties);
end
