
function ui2PopupBase(x, y, w, h)
	nvgSave();

	-- massive input region behind screen so nothing else can be clicked on.
	-- (could actually bing & flash popup if clicked away?)
	local m = mouseRegion(-viewport.width / 2, -viewport.height / 2, viewport.width, viewport.height);

	-- grey entire screen
	nvgBeginPath();
	nvgRect(-viewport.width / 2, -viewport.height / 2, viewport.width, viewport.height);
	nvgFillColor(Color(0, 0, 0, 128));
	nvgFill();	

	-- background for popup window
	nvgBeginPath();
	nvgRect(x, y, w, h);
	nvgFillColor(Color(20, 20, 20, 255));
	nvgFill();

	-- drop shadow
	local cornerRadius = 3;
	nvgBeginPath();
	nvgRect(x - 10, y - 10, w + 20, h + 30);
	nvgRoundedRect(x, y, w, h, cornerRadius);
	nvgPathWinding(NVG_HOLE);
	nvgFillBoxGradient(
		x, y + 2, w, h, cornerRadius * 2, 10,
		Color(0, 0, 0, 128),
		Color(0, 0, 0, 0));
	nvgFill();

	nvgRestore();
end