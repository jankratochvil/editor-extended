
function uiVerticalLabel(text, x, y, orientation, enabled)
	local h = 35;

	-- enabled is optional (so it can be nil)
	local c = 255;
	if enabled == false then c = UI_DISABLED_TEXT; end;

	nvgSave();

	nvgFontSize(FONT_SIZE_DEFAULT);
	nvgFontFace(FONT_TEXT);
	nvgFillColor(Color(c, c, c, 128));

	if orientation == "left" then
		nvgTextAlign(NVG_ALIGN_CENTER, NVG_ALIGN_MIDDLE);
		nvgTranslate(x,y);
		nvgRotate(-math.pi/2);
		nvgText(-h/2, 0, text);
	elseif orientation == "right" then 
		nvgTranslate(x,y);
		nvgRotate(math.pi/2);
		nvgText(h/2, 0, text);
	end

	nvgRestore();
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

function uiSliderVertical(x, y, w, min, max, value, optionalId, enabled)
	--edited original slider by FTr

	-- want consistent heights really
	local h = 35;

	-- pos 0->1
	local range = (max - min);
	local pos = (value - min) / range;
	pos = math.max(pos, 0);
	pos = math.min(pos, 1); --make sure its in <0,1>


	--simplified variables etc

	local slotWidth = 5;
	local centerX = x + slotWidth/2;
	local knobY = y + math.floor(pos*w);
	local knobRadius = math.floor(h*0.25);

	-- mouse stuff
	local m = {};
	if enabled == false then
		m.hoverAmount = 0;
		m.leftUp = false;
		m.leftHeld = false;
	else
		--either use entire slider area and compromise between knob size and bar size
		-- m = mouseRegion(x,y,knobRadius*1.5,w, optionalId);

		--or use only knob area
		m = mouseRegion(centerX - knobRadius, knobY - knobRadius, knobRadius*2, knobRadius*2, optionalId);
	end

	nvgSave();

	local knobColor = 255;
	local slotAlpha = 128;
	local borderAlpha = 92;
	if enabled == false then
		knobColor = 50;
		slotAlpha = 32;
		borderAlpha = 32;
	end

	-- Slot
	nvgBeginPath();
	nvgRoundedRect(x, y, slotWidth, w, slotWidth/2);
	nvgFillBoxGradient(x, y, slotWidth, w, 0, slotWidth,
		Color(0, 0, 0, 32),
		Color(0, 0, 0, slotAlpha));
	nvgFill();

	--knob shadow
	nvgBeginPath();
	nvgRect(x,knobY, knobRadius*2, knobRadius*2);
	nvgCircle(centerX, knobY, knobRadius);
	nvgPathWinding(NVG_HOLE);
	nvgFillRadialGradient(
		centerX, knobY, knobRadius-3, knobRadius+3,
		Color(0,0,0,64),
		Color(0,0,0,0));
	nvgFill();

	--Knob
	nvgBeginPath();
	nvgCircle(centerX, knobY, knobRadius);
	nvgFillColor(Color(40, 43, 48, 255));
	nvgFill();
	nvgFillLinearGradient(
		centerX, knobY - knobRadius, centerX, knobY + knobRadius, 
		Color(knobColor, knobColor, knobColor, 16),
		Color(0, 0, 0, 16));
	nvgFill();
	
	-- default border colour
	local bc = Color(0,0,0,borderAlpha);
	-- modify when hovering
	bc.r = lerp(bc.r, UI_HOVER_BORDER_COLOR.r, m.hoverAmount);
	bc.g = lerp(bc.g, UI_HOVER_BORDER_COLOR.g, m.hoverAmount);
	bc.b = lerp(bc.b, UI_HOVER_BORDER_COLOR.b, m.hoverAmount);
	bc.a = lerp(bc.a, UI_HOVER_BORDER_COLOR.a, m.hoverAmount);

	--border
	nvgBeginPath();
	nvgCircle(centerX, knobY, knobRadius-0.5);
	nvgStrokeColor(bc);
	nvgStroke();

	nvgRestore();

	if m.leftHeld then
		-- get the desired position in Nx1080 viewport
		local desiredKnobX = m.mousex;
		local desiredKnobY = m.mousey;

		-- convert this into desiredValue
		-- local desiredValue = ((desiredKnobX - x) / w) * range + min;
		local desiredValue = ((desiredKnobY - y) / w) * range + min;

		-- and store
		value = desiredValue;
		value = math.min(value, max);
		value = math.max(value, min);
	end
	return value;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

function uiQmTooltip(x, y, text, enabled, anchor, screenSize)
	--created by FTr
	-- creates question mark in a circle for menu usage
	-- uses uiToolTip

	local h = 35;

	local qmarkSize = 22;
	local colorAlpha = 128;
	local fontColor = Color(150,150,150,colorAlpha);
	local radius = 18;

	local px = x + 1;
	local py = y + math.floor(h*0.5) - 9;
	local pr = 22;

	-- mouse stuff
	local m = {};
	if enabled == false then
		m.hoverAmount = 0;
		m.leftUp = false;
		m.leftHeld = false;
	else
		--our tooltip location
		-- +0.5 to compensate for thicker stroke
		m = mouseRegion(px, py, radius+0.5, radius+0.5, optionalId);
	end

	--ripped from checkbox
	nvgBeginPath();
	nvgRoundedRect(px, py, radius, radius, 8);
	nvgFillBoxGradient(px, py + 1, radius, radius, 3, 3, Color(0, 0, 0, 32), Color(0, 0, 0, 92));
	nvgFill();

	local c = Color(0,0,0,0);
	if m.hover then
		c.r = lerp(c.r, UI_HOVER_BORDER_COLOR.r, m.hoverAmount);
		c.g = lerp(c.g, UI_HOVER_BORDER_COLOR.g, m.hoverAmount);
		c.b = lerp(c.b, UI_HOVER_BORDER_COLOR.b, m.hoverAmount);
		c.a = lerp(c.a, UI_HOVER_BORDER_COLOR.a, m.hoverAmount);

		fontColor.r = lerp(fontColor.r, UI_HOVER_BORDER_COLOR.r, m.hoverAmount);
		fontColor.g = lerp(fontColor.g, UI_HOVER_BORDER_COLOR.g, m.hoverAmount);
		fontColor.b = lerp(fontColor.b, UI_HOVER_BORDER_COLOR.b, m.hoverAmount);
		fontColor.a = lerp(fontColor.a, UI_HOVER_BORDER_COLOR.a, m.hoverAmount);

		nvgStrokeColor(c);
		nvgStrokeWidth(1);
		nvgStroke();

		--use edited tooltip to display the text
		-- include hoverAmount to smoothly fade in
		-- todo - fade out ?
		uiToolTipFading(x,y-20,text,m.hoverAmount, anchor, screenSize);
	-- else
	-- 	nvgStrokeColor(Color(0,0,0,150));
	-- 	nvgStrokeWidth(1.5);
	-- 	nvgStroke();
	end

	--display question mark
	nvgTextAlign(NVG_ALIGN_CENTER,NVG_ALIGN_MIDDLE);
	nvgFillColor(fontColor);
	nvgFontSize(qmarkSize);
	nvgText(x+10-0.3,y+math.floor(h/2),'?');
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

function string:split( inSplitPattern, outResults ) --stole this from the Internet
  if not outResults then
    outResults = { }
  end
  local theStart = 1
  local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
  while theSplitStart do
    table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
    theStart = theSplitEnd + 1
    theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
  end
  table.insert( outResults, string.sub( self, theStart ) )
  return outResults
end

    -- uiQmTooltip(x+250,y,QMTOOLTIP_FLIPTEXT);
    -- user.bFlipText= uiCheckBox(user.bFlipText, "Flip text automatically", x+250, y);

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

function uiCheckBoxTooltip(x, y, toolTipText, label, value, optionalId, enabled, anchor, screenSize)
	anchor = anchor or 'c'; --default value = center
	uiQmTooltip(x, y, toolTipText, enabled, anchor, screenSize);
	return uiCheckBox(value, label, x, y, optionalId, enabled);
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

function uiToolTipFading(x, y, text, hoverAmount, anchor, screenSize)
	local pad = 3;
	local fontSize = FONT_SIZE_SMALL;

	x = x + 15;

	nvgFontSize(fontSize);
	nvgFontFace(FONT_TEXT_BOLD);
	nvgTextAlign(NVG_ALIGN_LEFT, NVG_ALIGN_TOP);
	
	local bounds = nvgTextBounds("0");
	local boundsHeight = bounds.maxy - bounds.miny;

	nvgSave();	

	local textTable = text:split("\n"); --create table with separated strings

	--find text bounds
	for i = 1, #textTable do 
		local new = nvgTextBounds(textTable[i]);
		if new.maxx > bounds.maxx then 
			-- if new text is wider, replace it in bounds
			bounds.maxx = new.maxx 
		end
		--add boundsHeight if this is not last item
		if i ~= #textTable then bounds.maxy = bounds.maxy + boundsHeight end
	end

	if anchor == 'w' then
		x = x - rectw;
	elseif anchor == 'c' then
		x = x - rectw/2;
	elseif anchor == 'e' then
		x = x;
	end

	local rectw = bounds.maxx - bounds.minx + pad * 2;
	local recth = bounds.maxy - bounds.miny + pad * 2;

	-- screenSize: x, y, ex, ey
	if screenSize then
		if screenSize.x > x then x = screenSize.x + 10; end
		if screenSize.ex < x+rectw then x = screenSize.ex - rectw - 10; end
		if screenSize.y-recth < y then y = screenSize.y + recth + 10; end
		if screenSize.ey < y+recth then y = screenSize.ey - recth - 10; end
	end

	--modify Y value to always start at the top
	y = y - bounds.maxy-bounds.miny + boundsHeight;

	local rectx = x + bounds.minx - pad;
	local recty = y + bounds.miny - pad;

	--make sure the tooltip won't be displayed outside the window
	--failure
	--[[
	consolePrint(math.abs(rectx)+rectw);
	consolePrint(viewport.width);
	if math.abs(rectx) + rectw > viewport.width then
		rectx = viewport.width - rectw - 20;
	end
	]]--


	nvgBeginPath();
	nvgRoundedRect(rectx, recty, rectw, recth, 3);
	local col = Color(30, 30, 30, lerp(0,230,hoverAmount));
	nvgFillColor(col);
	nvgFill(vg);
	nvgStrokeColor(Color(200, 200, 30, lerp(0,170,hoverAmount)));
	nvgStrokeWidth(.5);
	nvgStroke();

	for i = 1, #textTable do
		nvgFontBlur(2);
		nvgFillColor(Color(0, 0, 0, lerp(0,128,hoverAmount)));
		nvgText(x, y, textTable[i]);
		
		nvgFontBlur(0);
		nvgFillColor(Color(220, 220, 220, lerp(0,160,hoverAmount)));
		nvgText(x, y, textTable[i]);
		y = y + fontSize;
	end
	nvgRestore();
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
